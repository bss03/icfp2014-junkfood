### What is this repository for? ###

This is the Team Junk Food submission for the ICFP 2014 [Programming Contest](http://icfpcontest.org/).  As I write this, we don't know what task will need to be accomplished.

### How do I get set up? ###

1. Install cabal, version 1.18 or greater (for sandboxes).  You will also need a Haskell compiler.
1. Clone the repository.
1. `(cabal build)`
1. Happy hacking!

### Contribution guidelines ###

If you have access to the repository at this time, you have full write access.  Please follow the [Wheaton's Law](http://knowyourmeme.com/memes/wheatons-law).

You retain copyright to all your contributions, please be sure to add your name to the copyright in both individual files you change and the `icfp2014.cabal` file.

### Who do I talk to? ###

* Repo owner or admin
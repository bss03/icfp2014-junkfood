{-# LANGUAGE DeriveFunctor #-}
module LabeledInstruction where

import qualified Data.Map as M
import Data.Maybe (catMaybes)

data InstructionOrLabel addr =
	  Label addr
	| ADD
	| AP Integer
	| ATOM
	| BRK
	| CAR
	| CDR
	| CEQ
	| CGT
	| CGTE
	| CONS
	| DBUG
	| DIV
	| DUM Integer
	| JOIN
	| LDC Integer
	| LDF addr
	| LD Integer Integer
	| MUL
	| RAP Integer
	| RTN
	| SEL addr addr
	| ST Integer Integer
	| STOP
	| SUB
	| TAP Integer
	| TRAP Integer
	| TSEL addr addr
	deriving (Eq, Read, Show, Functor)

reduceLabels :: Ord label => [InstructionOrLabel label] -> [InstructionOrLabel Int]
reduceLabels []           = []
reduceLabels instrs@(h:t) = map (cvt . snd) labeledInstr
 where
	labeledInstr = filter isInstr $ (Nothing, h) : (map lblPair $ zip instrs t)
	 where
		lblPair (Label l, i) = (Just l, i)
		lblPair (_      , i) = (Nothing, i)
		isInstr (_, Label _) = False
		isInstr _            = True
	lblMap = M.fromList . catMaybes $ zipWith labelAddr labeledInstr [0..]
	 where
		labelAddr (Just l, _) a = Just (l, a)
		labelAddr _            _ = Nothing
	addrLabel l = M.findWithDefault (error "Missing Label") l lblMap
	cvt = fmap addrLabel

{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE DeriveFunctor, DeriveFoldable, DeriveTraversable #-}
-- | Free magmas over a set, or at least parts.
module Magma.Free
	( MagmaF(Element, Operation)
	, FreeMagma
	)
where

-- base
import Data.Data (Data)
import Data.Foldable (Foldable)
import Data.Traversable (Traversable)
import Data.Typeable (Typeable)

-- recursion-schemes
import Data.Functor.Foldable (Fix)

-- | Base functor for free magmas over a set.
data MagmaF a f = Element a     -- ^ Single item
                | Operation f f -- ^ Apply magma operation
                deriving
                ( Eq, Ord
                , Read, Show
                , Data, Typeable
                , Functor, Foldable, Traversable
                )

-- | Free magma over a set.
type FreeMagma a = Fix (MagmaF a)

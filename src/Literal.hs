module Literal
	( Lit, Literal(..)
	, litInt, litCons
	, enumToLit
	, cataLit
	)
where

-- recursion-schemes
import Data.Functor.Foldable (cata, embed)

import Magma.Free (FreeMagma, MagmaF(Element, Operation))

{-|
`Lit`erals are either 'Integer's or cons cells, which makes them a free magma
over Integers.
-}
type Lit = FreeMagma Integer

-- | "Smart" constructor
litInt :: Integer -> Lit
litInt = embed . Element

-- | "Smart" constructor
litCons :: Lit -> Lit -> Lit
litCons = (embed .) . Operation

-- | Catamorphism without directly accessing 'MagmaF' constructors.
cataLit :: (Integer -> a) -> (a -> a -> a) -> Lit -> a
cataLit inject magmaOp = cata mkAlg
 where
	mkAlg (Element i)     = inject i
	mkAlg (Operation x y) = x `magmaOp` y

-- | Typeclass, for non-primitive literals.
class Literal a where
	toLit  :: a -> Lit

instance Literal Integer where
	toLit = litInt

instance Literal Int where
	toLit = toLit . toInteger

-- | Helper, for implementing 'toLit' on any 'Enum' type via 'fromEnum'
enumToLit :: Enum a => a -> Lit
enumToLit = toLit . fromEnum

{- |
`[]` is represented as `0`. `(x:xs)` is represented as a cons cell:
`(x, xs)`.
-}
instance Literal a => Literal [a] where
	toLit = foldr (litCons . toLit) (litInt 0)

instance (Literal a, Literal b) => Literal (a, b) where
	toLit (x, y) = litCons (toLit x) (toLit y)

{- |
Tuples are always right-nested.  I.e. (x, y, z) is represented as
`(x, (y, z))`.
-}
instance (Literal a, Literal b, Literal c) => Literal (a, b, c) where
	toLit (x, y, z) = toLit (x, (y, z))

instance (Literal a, Literal b, Literal c, Literal d)
      => Literal (a, b, c, d) where
	toLit (w, x, y, z) = toLit (w, (x, y, z))

instance (Literal a, Literal b, Literal c, Literal d, Literal e)
      => Literal (a, b, c, d, e) where
	toLit (v, w, x, y, z) = toLit (v, (w, x, y, z))

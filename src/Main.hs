{-# LANGUAGE DeriveDataTypeable, DeriveFunctor, GADTs, RankNTypes, TypeOperators, TypeFamilies #-}
module Main (main) where

import LabeledInstruction
import Literal (Literal(toLit), enumToLit, cataLit)

import Control.Applicative (liftA3, (<$>), (<*>))
import Control.Monad.Trans.State.Strict (State, runState, gets, modify)

import Data.Data (Data)
import qualified Data.Map.Strict as M
import Data.Map.Strict (Map)
import Data.Typeable (Typeable)

data Exp t where
	Constant     :: Literal a => a -> Exp a
	Add          :: Exp Integer -> Exp Integer -> Exp Integer
	Subtract     :: Exp Integer -> Exp Integer -> Exp Integer
	Multiply     :: Exp Integer -> Exp Integer -> Exp Integer
	Divide       :: Exp Integer -> Exp Integer -> Exp Integer
	Equal        :: Exp Integer -> Exp Integer -> Exp Integer
	GreaterThan  :: Exp Integer -> Exp Integer -> Exp Integer
	GreaterEqual :: Exp Integer -> Exp Integer -> Exp Integer
	Atom         :: Exp a -> Exp Integer
	Cons         :: Exp a -> Exp b -> Exp (a, b)
	Car          :: Exp (a, b) -> Exp a
	Cdr          :: Exp (a, b) -> Exp b
	IfThenElse   :: Exp Integer -> Exp a -> Exp a -> Exp a
	Lambda       :: (Exp a -> Exp b) -> Exp (a -> b)
	Var          :: Integer -> Exp a -- weak type
	SetVar       :: Integer -> Exp a -> Exp b -> Exp b
	Apply        :: Exp (a -> b) -> Exp a -> Exp b
	Stop         :: Exp a
	Debug        :: Exp a -> Exp b -> Exp b
	Break        :: Exp a -> Exp a
	-- LD is Var
	-- SEL/JOIN is IfThenElse
	-- ST is SetVar
	-- LDF/AP/RTN is Lambda/Apply
	-- DUM/RAP is TBI
	-- TSEL/TAP/TRAP is TBI

compileLASM :: Exp a -> [InstructionOrLabel String]
compileLASM expMain = (mainASM ++) . concatMap (\(l, p) -> Label l : p) $ M.toList globals
 where
	(mainASM, globals) = runState (compileHelp expMain) M.empty
	compileHelp :: Exp a -> State (Map String [InstructionOrLabel String]) [InstructionOrLabel String]
	compileHelp (Constant l)       = return . asmLit $ toLit l
	 where
		asmLit = cataLit onInteger onCons
		 where
			onInteger i = [LDC i]
			onCons x y  = concat [x, y, [CONS]]
	compileHelp (Add l r)          = fmap concat $ sequence [compileHelp l, compileHelp r, return [ADD]]
	compileHelp (Subtract l r)     = fmap concat $ sequence [compileHelp l, compileHelp r, return [SUB]]
	compileHelp (Multiply l r)     = fmap concat $ sequence [compileHelp l, compileHelp r, return [MUL]]
	compileHelp (Divide   l r)     = fmap concat $ sequence [compileHelp l, compileHelp r, return [DIV]]
	compileHelp (Equal    l r)     = fmap concat $ sequence [compileHelp l, compileHelp r, return [CEQ]]
	compileHelp (GreaterThan l r)  = fmap concat $ sequence [compileHelp l, compileHelp r, return [CGT]]
	compileHelp (GreaterEqual l r) = fmap concat $ sequence [compileHelp l, compileHelp r, return [CGTE]]
	compileHelp (Atom e)           = fmap concat $ sequence [compileHelp e, return [ATOM]]
	compileHelp (Cons l r)         = fmap concat $ sequence [compileHelp l, compileHelp r, return [CONS]]
	compileHelp (Car p)            = fmap concat $ sequence [compileHelp p, return [CAR]]
	compileHelp (Cdr p)            = fmap concat $ sequence [compileHelp p, return [CDR]]
	compileHelp (IfThenElse c t e) = do
		tn <- newGlobal t [JOIN]
		en <- newGlobal e [JOIN]
		fmap concat $ sequence [compileHelp c, return [SEL tn en]]
	compileHelp (Lambda f)         = compileHelp (f $ Var 0)
	compileHelp (Var i)            = return [LD i 0]
	compileHelp (SetVar i v n)     = fmap concat $ sequence [compileHelp v, return [ST i 0], compileHelp n]
	compileHelp (Apply l a)        = do
		ln <- newGlobal l [RTN]
		fmap concat $ sequence [compileHelp a, return [LDF ln, AP 1]]
	compileHelp Stop               = return [STOP]
	compileHelp (Debug e n)        = fmap concat $ sequence [compileHelp e, return [DBUG], compileHelp n]
	compileHelp (Break n)          = fmap concat $ sequence [return [BRK], compileHelp n]
	newGlobal :: Exp a -> [InstructionOrLabel String] -> State (Map String [InstructionOrLabel String]) String
	newGlobal exp t = do
		tp <- compileHelp exp
		tn <- fmap (("lam" ++) . show) $ gets M.size
		modify (M.insert tn (tp ++ t))
		return tn

{- World state -}
data World = World
	{ wMap :: [[CellContent]] -- ^ 2D map
	, lmStatus :: LMStatus   -- ^ LambdaMan
	, gStatus :: [GStatus]   -- ^ Ghosts, order based on start location
	, fStatus :: Integer     -- ^ Fruit timer
	} deriving (Eq, Show, Read, Data, Typeable)
instance Literal World where
	toLit = toLit . ((,,,) <$> wMap <*> lmStatus <*> gStatus <*> fStatus)

{- Single cell on the static map -}
data CellContent = Wall
                 | Empty
                 | Pill      -- Nom for great points
                 | PowerPill -- Vitality booster
                 | Fruit     -- Where fruit appears, may not be there
                 | LMStart   -- Where LambdaMan starts, not current location
                 | GStart    -- Where ghost start, not where they are
	deriving (Eq, Ord, Show, Read, Enum, Bounded, Data, Typeable)
instance Literal CellContent where
	toLit = enumToLit

{- Status of LambdaMan -}
data LMStatus = LMStatus
	{ lmVitality :: Integer            -- "Chomp" timer
	, lmLocation :: (Integer, Integer) -- (x, y)
	, lmDirection :: Direction         -- Desired movement
	, lives :: Integer                 -- When out, game over
	, score :: Integer                 -- Optimize for highest
	} deriving (Eq, Show, Read, Data, Typeable)
instance Literal LMStatus where
	toLit = toLit . ((,,,,) <$> lmVitality <*> lmLocation <*> lmDirection <*> lives <*> score)

{- Status of a single ghost -}
data GStatus = GStatus
	{ gVitality :: GVitality
	, gLocation :: (Integer, Integer) -- (x, y)
	, gDirection :: Direction         -- desired movement
	} deriving (Eq, Show, Read, Data, Typeable)
instance Literal GStatus where
	toLit = toLit . liftA3 (,,) gVitality gLocation gDirection

{- Enumerated "vitality" of a ghost -}
data GVitality = Standard  -- ^ Normal, dangerous state
               | Fright    -- ^ Frighted, edible state
               | Invisible -- ^ Eaten, intangible state
	deriving (Eq, Ord, Show, Read, Enum, Bounded, Data, Typeable)
instance Literal GVitality where
	toLit = enumToLit

{- Enuemrated direction of motion on 2D map. -}
data Direction = Up | Right | Down | Left
	deriving (Eq, Ord, Show, Read, Enum, Bounded, Data, Typeable)
instance Literal Direction where
	toLit = enumToLit

type Step s = s -> World -> (s, Direction)

{- Type of AI main function, if implemented in Haskell -}
type Main s = World -> Undocumented -> (s, Step s)

{-| type of undocumented second argument to AI main. -}
data Undocumented

test = Apply (Lambda (\x -> Add x x)) (Constant 21)

main :: IO ()
main = mapM_ print . reduceLabels $ compileLASM test

{-
Copyright © 2014 Boyd Stephen Smith Jr.

This file is part of icfp2014-junkfood.

icfp2014-junkfood is free software: you can redistribute it and/or modify it
under the terms of the GNU Affero General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your option)
any later version.

icfp2014-junkfood is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
details.

You should have received a copy of the GNU Affero General Public License along
with Foobar.  If not, see <http://www.gnu.org/licenses/>.
-}
